﻿const express = require('express');

const currencyPackage = express.Router();
const currencyController = require('../controller/currencyController');

function routes() {

    const controller = currencyController(true);

    currencyPackage.route('/currency')
        .get(controller.get);
    currencyPackage.route('/currency/exchangeCurrency')
        .get(controller.exchangeCurrency);
    currencyPackage.route('/currency/checkCurrency')
        .get(controller.checkCurrency);

    return currencyPackage;
}

module.exports = routes;