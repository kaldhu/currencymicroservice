﻿const fetch = require('node-fetch');
function currencyController(enableCaching) {

    let currencyData = {};
    let currencyDataExpires = 0;
    
    function isEmptyObject(obj) {
        return !Object.keys(obj).length;
    }

    async function updateCurrencyData() {
        if (isEmptyObject(currencyData) || currencyDataExpires < Date.now()) {

            const currencyAPI = process.env.currencyAPI
            console.log(currencyAPI);
            const response = await fetch(currencyAPI);
           
            if (enableCaching) {
                const cacheControlString = await response.headers.get('Cache-Control');
                const maxAgeString = 'max-age=';
                const firstSplitIndex = cacheControlString.indexOf(maxAgeString) + maxAgeString.length;
                const splitLength = cacheControlString.indexOf(',', firstSplitIndex) - firstSplitIndex;

                const value = splitLength < 0 ?
                    parseInt(cacheControlString.substr(firstSplitIndex)) :
                    parseInt(cacheControlString.substr(firstSplitIndex, splitLength));

                currencyDataExpires = new Date();
                currencyDataExpires.setSeconds(currencyDataExpires.getSeconds() + value);
                console.log(`cached currency data until ${currencyDataExpires}`)
            }

            const json = await response.json();
            currencyData = json.rates;
        }
    }

    async function get(request, result) {
        await updateCurrencyData();
        return result.json(currencyData);
    }

    async function checkCurrency(request, result) {
        const { query } = request;
        await updateCurrencyData();

        return result.json(currencyData.hasOwnProperty(query['currency']));
    }

    async function exchangeCurrency(request, result) {
        const { query } = request;
        await updateCurrencyData();

        const currency = query['currency'];
        let amount = query['amount'];
        if (currencyData.hasOwnProperty(currency)) {
            amount =  amount * currencyData[currency];
        }
        else {
            return result.status(400)
            //TODO: Return error
        }
        return result.json(amount.toFixed(2));
    }
    return { get, exchangeCurrency, checkCurrency};
}

module.exports = currencyController;