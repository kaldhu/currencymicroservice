# ProductPackages
Created by: Phillip Smith
Email: kaldhu@yahoo.com

This site is currently being hosted at https://currencymicroservice.azurewebsites.net.

uses 3rd party API "Exchange Rates API" to get Exchange rate information, this is hosted at https://api.exchangeratesapi.io/

The Api is accessed via 

```
    /api/currency/
        > get
        > checkCurrency?currency=[currency code]
        > exchangeCurrency?currency=[currency code]&amount=[amount]

```
